import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import {Http, Headers} from '@angular/http';

import { LoadingController } from 'ionic-angular';

import { Exercicios } from '../exercicios/exercicios';

@Component({
  selector: 'page-treinos',
  templateUrl: 'treinos.html'
})
export class Treinos {

  data = this.navParams.get('data');
  treino1 = this.data.treinos.TREINO1;
  treino2 = this.data.treinos.TREINO2;
  treino3 = this.data.treinos.TREINO3;
  treino4 = this.data.treinos.TREINO4;
  treino5 = this.data.treinos.TREINO5;

  id1 = this.data.treinos.ID1;
  id2 = this.data.treinos.ID2;
  id3 = this.data.treinos.ID3;
  id4 = this.data.treinos.ID4;
  id5 = this.data.treinos.ID5;
  loader:any;

  carregando() {
    this.loader = this.loadingCtrl.create({
      content: "Carregando..."
    });
    this.loader.present();
  }

  carregou() {
    this.loader.dismiss();
  }

  erro(){
    this.loader.dismiss();
    alert("Verifique sua conexão com a internet.");
  }

  exercicios = function(data){
    this.carregou();
    this.navCtrl.push(Exercicios, {
      data: data
    });
  }

  treino = function(i){
    this.carregando();
    var post = "i=" + i;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.fazoferta.com.br/Api/exercicios', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.exercicios(data),
      err => this.erro(),
    );
  }
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController) {

  }

}
