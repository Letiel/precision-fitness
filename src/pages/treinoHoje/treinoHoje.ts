import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import {Http, Headers} from '@angular/http';

import 'rxjs/add/operator/map';

import { Login } from '../login/login';

@Component({
  selector: 'page-treinoHoje',
  templateUrl: 'treinoHoje.html'
})
export class TreinoHoje {

  modalidade:any;
  nome_exercicio:any;
  nomecurto:any;
  email:any;
  id:any;

  proximo_modalidade:any;
  proximo_nome_exercicio:any;

  validar_login = function(data){
    if(data.logado == 's'){
      this.modalidade = data.treino_atual.MODALIDADE;
      this.nome_exercicio = data.treino_atual.EXERCICIO;
      this.nomecurto = data.treino_atual.NOMECURTO;
      this.email = data.treino_atual.EMAIL;
      this.id = data.id;
      this.proximo_modalidade = data.proximo.MODALIDADE;
      this.proximo_nome_exercicio = data.proximo.EXERCICIO;
    }else{
      this.navCtrl.pop();
      this.navCtrl.setRoot(Login, {
        data: data
      });
    }
  }

  constructor(public navCtrl: NavController, public http: Http) {
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.fazoferta.com.br/Api/treinoHoje', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.validar_login(data),
      err => alert(err +"Verifique sua conexão com a internet."),
    );
  }

}
