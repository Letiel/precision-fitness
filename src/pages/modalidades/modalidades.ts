import { Component } from '@angular/core';

import { NavController, MenuController, LoadingController } from 'ionic-angular';

import {Http, Headers} from '@angular/http';

import { Login } from '../login/login';

import { Treinos } from '../treinos/treinos';

@Component({
  selector: 'page-modalidades',
  templateUrl: 'modalidades.html'
})
export class Modalidades {
  mod1:any;
  mod2:any;
  mod3:any;
  mod4:any;
  mod5:any;
  id1:any;
  id2:any;
  id3:any;
  id4:any;
  id5:any;
  loader:any;

  carregando() {
    this.loader = this.loadingCtrl.create({
      content: "Carregando conteúdo..."
    });
    this.loader.present();
  }

  carregou() {
    this.loader.dismiss();
  }

  erro(){
    this.loader.dismiss();
    alert("Verifique sua conexão com a internet.");
  }

  treino = function(data){
    this.carregou();
    this.navCtrl.push(Treinos, {
      data: data
    });
  }

  modalidade = function(i){
    this.carregando();
    var post = "i=" + i;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.fazoferta.com.br/Api/treinos', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.treino(data),
      err => this.erro(),
    );
  }

  validar_login = function(data){
    if(data.logado == 's'){
      this.mod1 = data.modalidades.MOD1;
      this.mod2 = data.modalidades.MOD2;
      this.mod3 = data.modalidades.MOD3;
      this.mod4 = data.modalidades.MOD4;
      this.mod5 = data.modalidades.MOD5;
      this.id1 = data.modalidades.ID1;
      this.id2 = data.modalidades.ID2;
      this.id3 = data.modalidades.ID3;
      this.id4 = data.modalidades.ID4;
      this.id5 = data.modalidades.ID5;
      this.carregou();
    }else{
      this.carregou();
      this.navCtrl.pop();
      this.navCtrl.setRoot(Login, {
        data: data
      });
    }
    this.carregou();
  }

  constructor(public navCtrl: NavController, public http: Http, public loadingCtrl: LoadingController, public menuCtrl: MenuController) {
    this.menuCtrl.enable(true, 'menu');
    this.carregando();
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.fazoferta.com.br/Api/modalidades', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.validar_login(data),
      err => this.erro(),
    );
  }

}
