import { Component } from '@angular/core';

import { NavController, LoadingController, AlertController } from 'ionic-angular';

import {Http, Headers} from '@angular/http';

import { Login } from '../login/login';

@Component({
  selector: 'page-contas',
  templateUrl: 'contas.html'
})
export class Contas {

  data:any;

  loader:any;

  carregando() {
    this.loader = this.loadingCtrl.create({
      content: "Carregando conteúdo.."
    });
    this.loader.present();
  }

  carregou() {
    this.loader.dismiss();
  }

  erro(){
    this.loader.dismiss();
    alert("Verifique sua conexão com a internet.");
  }

  validar_login = function(data){
    if(data.logado == 'n'){
      this.carregou();
      this.navCtrl.setRoot(Login);
    }else{
      this.data = data.contas
    }
    this.carregou();
  }

  mais(conta){
    let alert = this.alertCtrl.create({
      title: "R$"+conta.VALOR,
      subTitle: conta.TITULO,
      buttons: ['FECHAR']
    });
    alert.present();
  }

  constructor(public navCtrl: NavController, public http: Http, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
      this.carregando();
      var post = "";
      var headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      this.http.post('http://www.fazoferta.com.br/Api/contas', post, {
        headers: headers
      }).map(res => res.json())
      .subscribe(
        data => this.validar_login(data),
        err =>this.erro(),
      );
  }

}
