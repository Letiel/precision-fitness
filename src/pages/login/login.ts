import { Component } from '@angular/core';

import { NavController, LoadingController, MenuController } from 'ionic-angular';

import {Http, Headers} from '@angular/http';

import 'rxjs/add/operator/map';

import {Md5} from 'ts-md5/dist/md5';

// import { TreinoHoje } from '../treinoHoje/treinoHoje';
import { Modalidades } from '../modalidades/modalidades';

import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class Login {
  storage: Storage = new Storage();
  public login: any;
  senha: any;
  loader:any;

  carregando() {
    this.loader = this.loadingCtrl.create({
      content: "Carregando conteúdo..."
    });
    this.loader.present();
  }

  carregou() {
    this.loader.dismiss();
  }


  validar_login = function(data){
    if(data.logado=='s'){
      this.carregou();
      this.storage.set('email', data.login);
      this.navCtrl.pop();
      this.navCtrl.setRoot(Modalidades, {
        data: data
      });
    }else if(data.encontrado == "n"){
      this.carregou();
      alert(data.msg);
    } else if(data.login='s') {
      this.carregou();
      this.navCtrl.pop();
      this.navCtrl.setRoot(Login, {
        data: data
      });
    } else {
      this.carregou();
      alert("Ocorreu um erro.");
    }
    this.carregou();
  }

  testar_logado = function(data){
    if(data.logado=='s'){
      this.carregou();
      this.navCtrl.pop();
      this.navCtrl.setRoot(Modalidades, {
        data: data
      });
    }
    this.carregou();
  }

  erro(){
    this.loader.dismiss();
    alert("Verifique sua conexão com a internet.");
  }

  entrar = function(){
    this.carregando();
    var post = "email=" + this.login + "&senha=" + Md5.hashStr(this.senha);
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.fazoferta.com.br/Api/login', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.validar_login(data),
      err => this.erro(),
    );
  }

  constructor(public navCtrl: NavController, public http: Http, public loadingCtrl: LoadingController, public menuCtrl: MenuController) {
    this.menuCtrl.enable(false, 'menu');
    this.carregando();
    this.storage.get('email').then((email) => {
      this.login = email;
    });
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.fazoferta.com.br/Api/teste_login', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.testar_logado(data),
      err => this.erro(),
    );
  }

}
