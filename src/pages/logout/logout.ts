import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

import {Http, Headers} from '@angular/http';
import { Login } from '../login/login';

@Component({
  selector: 'page-logout',
  templateUrl: 'logout.html'
})
export class Logout {

  loader:any;

  carregando() {
    this.loader = this.loadingCtrl.create({
      content: "Saindo..."
    });
    this.loader.present();
  }

  carregou() {
    this.loader.dismiss();
  }

  erro(){
    this.loader.dismiss();
    alert("Verifique sua conexão com a internet.");
  }

  redirect(){
    this.carregou();
    this.navCtrl.setRoot(Login);
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController) {
    this.carregando();
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.fazoferta.com.br/Api/logout', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.redirect(),
      err => this.erro(),
    );
  }

}
