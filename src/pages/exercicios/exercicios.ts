import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import { LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-exercicios',
  templateUrl: 'exercicios.html'
})
export class Exercicios {

  data = this.navParams.get('data');

  exercicio1 = this.data.exercicios.NOME1;
  peso1 = this.data.exercicios.PESO1;
  repeticao1 = this.data.exercicios.REPETICAO1;
  sessao1 = this.data.exercicios.SESSAO1;
  tempo1 = this.data.exercicios.TEMPO1;

  exercicio2 = this.data.exercicios.NOME2;
  peso2 = this.data.exercicios.PESO2;
  repeticao2 = this.data.exercicios.REPETICAO2;
  sessao2 = this.data.exercicios.SESSAO2;
  tempo2 = this.data.exercicios.TEMPO2;

  exercicio3 = this.data.exercicios.NOME3;
  peso3 = this.data.exercicios.PESO3;
  repeticao3 = this.data.exercicios.REPETICAO3;
  sessao3 = this.data.exercicios.SESSAO3;
  tempo3 = this.data.exercicios.TEMPO3;

  exercicio4 = this.data.exercicios.NOME4;
  peso4 = this.data.exercicios.PESO4;
  repeticao4 = this.data.exercicios.REPETICAO4;
  sessao4 = this.data.exercicios.SESSAO4;
  tempo4 = this.data.exercicios.TEMPO4;

  exercicio5 = this.data.exercicios.NOME5;
  peso5 = this.data.exercicios.PESO5;
  repeticao5 = this.data.exercicios.REPETICAO5;
  sessao5 = this.data.exercicios.SESSAO5;
  tempo5 = this.data.exercicios.TEMPO5;

  exercicio6 = this.data.exercicios.NOME6;
  peso6 = this.data.exercicios.PESO6;
  repeticao6 = this.data.exercicios.REPETICAO6;
  sessao6 = this.data.exercicios.SESSAO6;
  tempo6 = this.data.exercicios.TEMPO6;

  exercicio7 = this.data.exercicios.NOME7;
  peso7 = this.data.exercicios.PESO7;
  repeticao7 = this.data.exercicios.REPETICAO7;
  sessao7 = this.data.exercicios.SESSAO7;
  tempo7 = this.data.exercicios.TEMPO7;

  exercicio8 = this.data.exercicios.NOME8;
  peso8 = this.data.exercicios.PESO8;
  repeticao8 = this.data.exercicios.REPETICAO8;
  sessao8 = this.data.exercicios.SESSAO8;
  tempo8 = this.data.exercicios.TEMPO8;

  exercicio9 = this.data.exercicios.NOME9;
  peso9 = this.data.exercicios.PESO9;
  repeticao9 = this.data.exercicios.REPETICAO9;
  sessao9 = this.data.exercicios.SESSAO9;
  tempo9 = this.data.exercicios.TEMPO9;

  exercicio10 = this.data.exercicios.NOME10;
  peso10 = this.data.exercicios.PESO10;
  repeticao10 = this.data.exercicios.REPETICAO10;
  sessao10 = this.data.exercicios.SESSAO10;
  tempo10 = this.data.exercicios.TEMPO10;

  exercicio11 = this.data.exercicios.NOME11;
  peso11 = this.data.exercicios.PESO11;
  repeticao11 = this.data.exercicios.REPETICAO11;
  sessao11 = this.data.exercicios.SESSAO11;
  tempo11 = this.data.exercicios.TEMPO11;

  exercicio12 = this.data.exercicios.NOME12;
  peso12 = this.data.exercicios.PESO12;
  repeticao12 = this.data.exercicios.REPETICAO12;
  sessao12 = this.data.exercicios.SESSAO12;
  tempo12 = this.data.exercicios.TEMPO12;

  exercicio13 = this.data.exercicios.NOME13;
  peso13 = this.data.exercicios.PESO13;
  repeticao13 = this.data.exercicios.REPETICAO13;
  sessao13 = this.data.exercicios.SESSAO13;
  tempo13 = this.data.exercicios.TEMPO13;

  exercicio14 = this.data.exercicios.NOME14;
  peso14 = this.data.exercicios.PESO14;
  repeticao14 = this.data.exercicios.REPETICAO14;
  sessao14 = this.data.exercicios.SESSAO14;
  tempo14 = this.data.exercicios.TEMPO14;

  exercicio15 = this.data.exercicios.NOME15;
  peso15 = this.data.exercicios.PESO15;
  repeticao15 = this.data.exercicios.REPETICAO15;
  sessao15 = this.data.exercicios.SESSAO15;
  tempo15 = this.data.exercicios.TEMPO15;

  exercicio16 = this.data.exercicios.NOME16;
  peso16 = this.data.exercicios.PESO16;
  repeticao16 = this.data.exercicios.REPETICAO16;
  sessao16 = this.data.exercicios.SESSAO16;
  tempo16 = this.data.exercicios.TEMPO16;

  exercicio17 = this.data.exercicios.NOME17;
  peso17 = this.data.exercicios.PESO17;
  repeticao17 = this.data.exercicios.REPETICAO17;
  sessao17 = this.data.exercicios.SESSAO17;
  tempo17 = this.data.exercicios.TEMPO17;

  exercicio18 = this.data.exercicios.NOME18;
  peso18 = this.data.exercicios.PESO18;
  repeticao18 = this.data.exercicios.REPETICAO18;
  sessao18 = this.data.exercicios.SESSAO18;
  tempo18 = this.data.exercicios.TEMPO18;

  exercicio19 = this.data.exercicios.NOME19;
  peso19 = this.data.exercicios.PESO19;
  repeticao19 = this.data.exercicios.REPETICAO19;
  sessao19 = this.data.exercicios.SESSAO19;
  tempo19 = this.data.exercicios.TEMPO19;

  exercicio20 = this.data.exercicios.NOME20;
  peso20 = this.data.exercicios.PESO20;
  repeticao20 = this.data.exercicios.REPETICAO20;
  sessao20 = this.data.exercicios.SESSAO20;
  tempo20 = this.data.exercicios.TEMPO20;

  exercicio21 = this.data.exercicios.NOME21;
  peso21 = this.data.exercicios.PESO21;
  repeticao21 = this.data.exercicios.REPETICAO21;
  sessao21 = this.data.exercicios.SESSAO21;
  tempo21 = this.data.exercicios.TEMPO21;

  exercicio22 = this.data.exercicios.NOME22;
  peso22 = this.data.exercicios.PESO22;
  repeticao22 = this.data.exercicios.REPETICAO22;
  sessao22 = this.data.exercicios.SESSAO22;
  tempo22 = this.data.exercicios.TEMPO22;

  exercicio23 = this.data.exercicios.NOME23;
  peso23 = this.data.exercicios.PESO23;
  repeticao23 = this.data.exercicios.REPETICAO23;
  sessao23 = this.data.exercicios.SESSAO23;
  tempo23 = this.data.exercicios.TEMPO23;

  exercicio24 = this.data.exercicios.NOME24;
  peso24 = this.data.exercicios.PESO24;
  repeticao24 = this.data.exercicios.REPETICAO24;
  sessao24 = this.data.exercicios.SESSAO24;
  tempo24 = this.data.exercicios.TEMPO24;

  exercicio25 = this.data.exercicios.NOME25;
  peso25 = this.data.exercicios.PESO25;
  repeticao25 = this.data.exercicios.REPETICAO25;
  sessao25 = this.data.exercicios.SESSAO25;
  tempo25 = this.data.exercicios.TEMPO25;

  exercicio26 = this.data.exercicios.NOME26;
  peso26 = this.data.exercicios.PESO26;
  repeticao26 = this.data.exercicios.REPETICAO26;
  sessao26 = this.data.exercicios.SESSAO26;
  tempo26 = this.data.exercicios.TEMPO26;

  exercicio27 = this.data.exercicios.NOME27;
  peso27 = this.data.exercicios.PESO27;
  repeticao27 = this.data.exercicios.REPETICAO27;
  sessao27 = this.data.exercicios.SESSAO27;
  tempo27 = this.data.exercicios.TEMPO27;

  exercicio28 = this.data.exercicios.NOME28;
  peso28 = this.data.exercicios.PESO28;
  repeticao28 = this.data.exercicios.REPETICAO28;
  sessao28 = this.data.exercicios.SESSAO28;
  tempo28 = this.data.exercicios.TEMPO28;

  exercicio29 = this.data.exercicios.NOME29;
  peso29 = this.data.exercicios.PESO29;
  repeticao29 = this.data.exercicios.REPETICAO29;
  sessao29 = this.data.exercicios.SESSAO29;
  tempo29 = this.data.exercicios.TEMPO29;

  exercicio30 = this.data.exercicios.NOME30;
  peso30 = this.data.exercicios.PESO30;
  repeticao30 = this.data.exercicios.REPETICAO30;
  sessao30 = this.data.exercicios.SESSAO30;
  tempo30 = this.data.exercicios.TEMPO30;

  exercicio31 = this.data.exercicios.NOME31;
  peso31 = this.data.exercicios.PESO31;
  repeticao31 = this.data.exercicios.REPETICAO31;
  sessao31 = this.data.exercicios.SESSAO31;
  tempo31 = this.data.exercicios.TEMPO31;

  exercicio32 = this.data.exercicios.NOME32;
  peso32 = this.data.exercicios.PESO32;
  repeticao32 = this.data.exercicios.REPETICAO32;
  sessao32 = this.data.exercicios.SESSAO32;
  tempo32 = this.data.exercicios.TEMPO32;

  exercicio33 = this.data.exercicios.NOME33;
  peso33 = this.data.exercicios.PESO33;
  repeticao33 = this.data.exercicios.REPETICAO33;
  sessao33 = this.data.exercicios.SESSAO33;
  tempo33 = this.data.exercicios.TEMPO33;

  exercicio34 = this.data.exercicios.NOME34;
  peso34 = this.data.exercicios.PESO34;
  repeticao34 = this.data.exercicios.REPETICAO34;
  sessao34 = this.data.exercicios.SESSAO34;
  tempo34 = this.data.exercicios.TEMPO34;

  exercicio35 = this.data.exercicios.NOME35;
  peso35 = this.data.exercicios.PESO35;
  repeticao35 = this.data.exercicios.REPETICAO35;
  sessao35 = this.data.exercicios.SESSAO35;
  tempo35 = this.data.exercicios.TEMPO35;

  exercicio36 = this.data.exercicios.NOME36;
  peso36 = this.data.exercicios.PESO36;
  repeticao36 = this.data.exercicios.REPETICAO36;
  sessao36 = this.data.exercicios.SESSAO36;
  tempo36 = this.data.exercicios.TEMPO36;

  exercicio37 = this.data.exercicios.NOME37;
  peso37 = this.data.exercicios.PESO37;
  repeticao37 = this.data.exercicios.REPETICAO37;
  sessao37 = this.data.exercicios.SESSAO37;
  tempo37 = this.data.exercicios.TEMPO37;

  exercicio38 = this.data.exercicios.NOME38;
  peso38 = this.data.exercicios.PESO38;
  repeticao38 = this.data.exercicios.REPETICAO38;
  sessao38 = this.data.exercicios.SESSAO38;
  tempo38 = this.data.exercicios.TEMPO38;

  exercicio39 = this.data.exercicios.NOME39;
  peso39 = this.data.exercicios.PESO39;
  repeticao39 = this.data.exercicios.REPETICAO39;
  sessao39 = this.data.exercicios.SESSAO39;
  tempo39 = this.data.exercicios.TEMPO39;

  exercicio40 = this.data.exercicios.NOME40;
  peso40 = this.data.exercicios.PESO40;
  repeticao40 = this.data.exercicios.REPETICAO40;
  sessao40 = this.data.exercicios.SESSAO40;
  tempo40 = this.data.exercicios.TEMPO40;


  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController) {

  }

}
