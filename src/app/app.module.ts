import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Login } from '../pages/login/login';
import { Treinos } from '../pages/treinos/treinos';
import { Modalidades } from '../pages/modalidades/modalidades';
import { Exercicios } from '../pages/exercicios/exercicios';
import { Contas } from '../pages/contas/contas';
import { Logout } from '../pages/logout/logout';

@NgModule({
  declarations: [
    MyApp,
    Login,
    Treinos,
    Modalidades,
    Exercicios,
    Contas,
    Logout
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Login,
    Treinos,
    Modalidades,
    Exercicios,
    Contas,
    Logout
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
